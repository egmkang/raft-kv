module raft-kv

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/hashicorp/go-hclog v0.9.1
	github.com/hashicorp/raft v1.2.0
	github.com/hashicorp/raft-boltdb v0.0.0-20191021154308-4207f1bf0617
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
	go.etcd.io/bbolt v1.3.5
)
