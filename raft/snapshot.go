package raft

import (
	"github.com/hashicorp/raft"
	"go.etcd.io/bbolt"
)

type snapshot struct {
	db *bbolt.DB
}

func (this *snapshot) Persist(sink raft.SnapshotSink) error {
	return this.db.View(func(tx *bbolt.Tx) error {
		_, err := tx.WriteTo(sink)
		return err
	})
}
func (this *snapshot) Release() {
}
