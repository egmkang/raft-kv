// ------------------------------------------------------------
// Copyright (c) Microsoft Corporation.
// Licensed under the MIT License.
// ------------------------------------------------------------

package raft

import (
	"fmt"
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestEnsureDir(t *testing.T) {
	testDir := "_testDir"
	t.Run("create dir successfully", func(t *testing.T) {
		err := ensureDir(testDir)
		assert.NoError(t, err)
		err = os.Remove(testDir)
		assert.NoError(t, err)
	})

	t.Run("ensure the existing directory", func(t *testing.T) {
		err := os.Mkdir(testDir, 0700)
		assert.NoError(t, err)
		err = ensureDir(testDir)
		assert.NoError(t, err)
		err = os.Remove(testDir)
		assert.NoError(t, err)
	})

	t.Run("fails to create dir", func(t *testing.T) {
		file, err := os.Create(testDir)
		assert.NoError(t, err)
		log.Printf("%v", file)
		file.Close()
		err = ensureDir(testDir)
		assert.Error(t, err)
		err = os.Remove(testDir)
		assert.NoError(t, err)
	})
}

func TestRaftAddressForID(t *testing.T) {
	var raftAddressTests = []struct {
		in  []PeerInfo
		id  string
		out string
	}{
		{
			[]PeerInfo{
				{ID: "node0", Address: "127.0.0.1:3030"},
				{ID: "node1", Address: "127.0.0.1:3031"},
			},
			"node0",
			"127.0.0.1:3030",
		}, {
			[]PeerInfo{
				{ID: "node0", Address: "127.0.0.1:3030"},
			},
			"node1",
			"",
		},
	}

	for _, tt := range raftAddressTests {
		t.Run(fmt.Sprintf("find %s from %v", tt.id, tt.in), func(t *testing.T) {
			assert.Equal(t, tt.out, raftAddressForID(tt.id, tt.in))
		})
	}
}

func TestMakeRaftLogCommand(t *testing.T) {
	// arrange
	cmd := KVCommand{
		Key:   1,
		Value: 0,
	}

	// act
	cmdLog, _ := makeRaftLogCommand(CommandGet, cmd)

	commandType, cmd1 := unmarshalRaftLog(cmdLog)
	assert.EqualValues(t, CommandGet, commandType)
	assert.EqualValues(t, cmd, cmd1)
}
