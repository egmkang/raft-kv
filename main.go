package main

import (
	"raft-kv/logger"
	"raft-kv/raft"
	"raft-kv/server"
	"time"
)

var log = logger.NewLogger("raft-kv")

func main() {
	cfg := newConfig()
	raftServer := raft.NewServer(cfg.nodeID, cfg.raftPeerInfo, cfg.dir, cfg.raftBootStrap)
	if raftServer == nil {
		log.Fatal("failed to create raft server.")
		return
	}

	if err := raftServer.Start(nil); err != nil {
		log.Errorf("failed to start RaftServer, Error:%v", err)
		return
	}

	apiService := server.NewApiService(cfg.port, raftServer)

	exit := false

	go func() {
		if err := apiService.Start(); err != nil {
			log.Errorf("start http service fail, error:%v", err)
			exit = true
			return
		}
	}()

	for {
		time.Sleep(time.Second)
		if exit {
			break
		}
	}

}
